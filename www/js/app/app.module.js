(function() {
  'use strict';

  angular
    .module('NoltApp', ['ngMaterial', 'ui.router'])
    .config(mdConfig)
    .config(stateProvider);

  mdConfig.$inject = ['$mdThemingProvider'];
  stateProvider.$inject = ['$stateProvider', '$urlRouterProvider'];

  function mdConfig($mdThemingProvider) {
    $mdThemingProvider.theme('default')
      .primaryPalette('blue', {
        'default': '500',
        'hue-1': '100',
        'hue-2': '700',
        'hue-3': 'A100',
      })
      .accentPalette('pink', {
        'default': '200',
        'hue-1': '100',
        'hue-3': 'A100',
      });
  }

  function stateProvider($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/home');

    $stateProvider

      .state('home', {
        url: '/home',
        templateUrl: 'js/app/home/partial-home.html'
      })

      .state('media', {
        url: '/media',
        templateUrl: 'js/app/media/partial-media.html',
        params: {
          data: null,
        },
        controller: function($scope, $stateParams) {
					var self = $scope;
					self.data = $stateParams.data;
          console.log('$stateParams', $stateParams); // { id: 1, data: { foo: 'bar' }}
        }

      });

  }

})();