(function() {
  'use strict';

  angular
    .module('NoltApp')
		.directive('searchMedia', searchMedia);

  function searchMedia() {

    return {
      restrict: 'A',
      scope: {},
      controller: 'SearchCtrl',
      controllerAs: 'vm',
      templateUrl: 'js/app/search/search.view.tmpl.html',
      bindToController: true
    };

  }

})();