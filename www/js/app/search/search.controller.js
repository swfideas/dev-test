(function() {
  'use strict';
  angular
    .module('NoltApp')
    .controller('SearchCtrl', SearchCtrl);

  SearchCtrl.$inject = ['$timeout', '$scope', '$log', '$state', 'ApiService'];

  function SearchCtrl($timeout, $scope, $log, $state, ApiService) {

    var self = this;

    self.simulateQuery = false;
    self.isDisabled = false;
    self.debug = true;
    self.loading = false;
    self.selectedItemChange = selectedItemChange;
    self.searchTextChange = searchTextChange;
    self.clearSearch = clearSearch;
    self.viewMedia = viewMedia;

    function searchTextChange(text) {
      $log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
      $log.info('Item changed to ' + JSON.stringify(item));
    }

    function clearSearch(item) {
      // TODO: Catch a better way to give no data to user
      alert("Sorry! not found!");
    }

    function viewMedia(item) {
      $log.info('item', item);
      $state.go('media', {
        data: item
      });
    }

    $scope.$watch('vm.searchField', function(tmpStr) {
      if (!tmpStr || tmpStr.length == 0)
        return 0;
      $timeout(function() {
        if (tmpStr === self.searchField) {
          self.loading = true;
          ApiService.getSearch(tmpStr)
            .then(function(data) {
              // update the textarea
              self.responseData = data;
            }, function(err) {
              $log.info('errr', err);
            })
            .finally(function() {
              self.loading = false;
            });
        }
      }, 500);
    });

  }
})();