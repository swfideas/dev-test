'use strict';
var gulp = require('gulp');
var sass = require('gulp-sass');
var webserver = require('gulp-webserver');
var vendor = require('gulp-concat-vendor');

gulp.task('sass', function() {
  return gulp.src('./sass/**/*.scss')
    .pipe(sass.sync({
      outputStyle: 'compressed'
    }).on('error', sass.logError))
    .pipe(gulp.dest('./www/css'));
});

gulp.task('watch:sass', function() {
  gulp.watch('./sass/**/*.scss', ['sass']);
});

gulp.task('run:local', function() {
  gulp.src('./www/')
    .pipe(webserver({
			host: '0.0.0.0',
      port: 6639,
      livereload: true,
      directoryListing: true,
      open: true,
      fallback: 'index.html'
    }));
});

gulp.task('build:scripts', function() {
  	gulp.src('./bower_components/*')
		.pipe(vendor('vendor.js'))
		.pipe(gulp.dest('./www/js'));
});