(function() {
  'use strict';

  angular
    .module('NoltApp')
    .factory('ApiService', ApiService);

  ApiService.$inject = ['$http', '$log'];

  function ApiService($http, $log) {

    var api_url = 'https://netflixroulette.net/api/api.php';

    var service = {
      getSearch: getSearch,
    };

    return service;

    function getSearch(params) {

      var title = 'title=';
      // TODO : split params by comma to make year/title search
      if (params.indexOf(',') > -1) {
        var reg = new RegExp('^(?:19|20)\d{2}$');
        var params_arr = params.split(',');

      }



      var request_params = title + params;

      var request_url = api_url + '?' + request_params;

      return $http({
          method: 'GET',
          url: request_url
        })
        .then(function successCallback(response) {
          return response.data;
        }, function(errorResponse) {
          $log.error(errorResponse.data);
          return errorResponse.data;
        });

    }

  }
})();